//
//  Principal1ViewController.swift
//  ApiExamen
//
//  Created by sandy simbaña on 12/12/17.
//  Copyright © 2017 sandy simbaña. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper


class Principal1ViewController: UIViewController {

    @IBOutlet weak var Ingreso: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    

    @IBAction func Consultar(_ sender: Any) {
        
        if Ingreso.text != ""
        {
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let principalViewController = segue.destination as! PrincipalViewController
        
        principalViewController.tituloPelicula = Ingreso.text!
        
    }
}

