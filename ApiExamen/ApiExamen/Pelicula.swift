//
//  Pelicula.swift
//  ApiExamen
//
//  Created by sandy simbaña on 13/12/17.
//  Copyright © 2017 sandy simbaña. All rights reserved.
//

import Foundation
import ObjectMapper

class Pelicula: Mappable {
    
    var titulo: String?
    var año: String?
    var director: String?
     var genero: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map){
        titulo <- map["Title"]
        año <- map["Year"]
        director <- map["Director"]
        genero <- map["Genre"]

    }
    
}
