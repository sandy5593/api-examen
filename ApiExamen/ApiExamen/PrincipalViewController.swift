//
//  PrincipalViewController.swift
//  ApiExamen
//
//  Created by sandy simbaña on 12/12/17.
//  Copyright © 2017 sandy simbaña. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class PrincipalViewController: UIViewController {
    
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var año: UILabel!
    @IBOutlet weak var director: UILabel!
    @IBOutlet weak var genero: UILabel!
    
    
    
    var tituloPelicula = String()
    
    var v = [Pelicula]()
    var numeroResultado = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        Alamofire.request("http://www.omdbapi.com/?t=" + tituloPelicula + "&apikey=1f0444bf").responseObject{ (response: DataResponse<Pelicula>) in
            let pelicula = response.result.value
            
            DispatchQueue.main.async
                {
                    self.titulo.text = pelicula?.titulo
                    self.director.text = pelicula?.director
                     self.genero.text = pelicula?.genero
                   self.año.text = pelicula?.año
                    
            }
            
        }
       
    }
    
    
    @IBAction func regresar(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //override func didReceiveMemoryWarning() {
      //  super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    //}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
